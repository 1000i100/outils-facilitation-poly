# outils-facilitation-poly

## Présentaiton de l'évènement

Parlons relations, émotions, avec bienveillance et ouverture.

Que vous rencontriez des difficultés dans votre/vos relations intimes ou que vous souhaitiez témoigner du chemin qui vous permet de vivre un idyle, soyez les bienvenue.

CurieuSESx de la diversité et de la créativité des participantEs dans leur choix et modalité relationnels (polyamour, couple ouvert, anarchie relationnelle) ? Bienvenue.


## Déroulement proposé

1. Accueil
2. Présentation de l'évènement et des outils
3. Carillion (suit tes pieds)
4. Cloture
5. Off

### Discussions en groupe
Si vous êtes nombreux, vous risquez :
- d'avoir du mal à entendre tout le monde, surtout s'il y a du bruit au allentours
- d'être frustré de ne pas pouvoir vous exprimer autant que vous aimeriez

Si vous êtes très peu nombreux, vous risquez :
- une diversité des experiences vécues moindre

Je vous recommande de former des groupes de 4 à 10 personnes.

Sans thème plus spécifique que celui de l'évènement, il peut être délicat de savoir de quoi parler.
Je vous recommande de commencer par un tour de table ou dire avec concision votre experience du sujet et vos motivation à être ici (quête de réponses/témoignages sur un sujet précis, envie de transmettre...)

Si vous ne souhaitez pas prendre la parole (que ce soit lors d'un tour de table ou à un autre moment) nous vous proposons des **bulles barrées** à l'accueil
que vous pouvez porter et désigner du doigt pour expliciter que vous préférez écouter sans parler.

## Outils

### Attaches
Epingles à nourisse, épingles, trombonnes, épingle à linge, aimant, porte carte...

Vous devriez trouver à l'accueil des attaches pour pouvoir porter sur vous différentes informations à destination des autres participants.
Nous allons les détailler ci-dessous.

### Bulle barrée
